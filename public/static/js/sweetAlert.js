const { default: Swal } = require("sweetalert2");


Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
    }
  })


//   var btn_search =document.getElementById('btn_search');
//   var form_search =document.getElementById('btn_search');
//   if(btn-search){
//       btn_search.addEventListener('click',function(e){
//           e.preventDefault();
//           if(form_search.style.display === 'block'){
//               form_search.style.display = 'none';
//           }else{
//               form_search.style.display ='block';
//           }
//       });

//   }
