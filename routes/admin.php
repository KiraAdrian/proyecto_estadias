<?php

Route::prefix('/admin')->group(function(){
    Route::get('/','Admin\DashboardController@getDashboard')->name('dashboard');

    Route::get('/users/{status}','Admin\UserController@getUsers')->name('user_list');
    Route::get('/user/{id}/edit','Admin\UserController@getUserEdit')->name('user_edit');
    Route::post('/user/{id}/edit','Admin\UserController@postUserEdit')->name('user_edit');
    Route::get('/user/{id}/banned','Admin\UserController@getUserBanned')->name('user_banned');
    Route::get('/user/{id}/permissions','Admin\UserController@getUserPermissions')->name('user_permissions');
    Route::post('/user/{id}/permissions','Admin\UserController@postUserPermissions')->name('user_permissions');



    //productos
    Route::get('/products/{status}','Admin\ProductController@getHome')->name('products');
    Route::get('/product/add','Admin\ProductController@getProductAdd')->name('product_add');
    Route::get('/product/{id}/edit','Admin\ProductController@getProductEdit')->name('product_edit');
    Route::get('/product/{id}/delete','Admin\ProductController@getProductDelete')->name('product_delete');
    Route::get('/product/{id}/restore','Admin\ProductController@getProductRestore')->name('product_delete');
    Route::post('/product/add','Admin\ProductController@postProductAdd')->name('product_add');
    Route::post('/product/{id}/edit','Admin\ProductController@postProductEdit')->name('product_edit');
    Route::post('/product/search','Admin\ProductController@postProductSearch')->name('product_search');
    Route::post('/product/{id}/gallery/add','Admin\ProductController@postProductGalleryAdd')->name('product_gallery_add');
    Route::get('/product/{id}/gallery/{gid}/delete','Admin\ProductController@getProductGalleryDelete')->name('product_gallery_delete');

    //Mantenimientos
    Route::get('/mantenimientos/{status}','Admin\MantenimientoController@getHome')->name('mantenimientos');
    Route::get('/mantenimiento/add','Admin\MantenimientoController@getMantenimientoAdd')->name('mantenimiento_add');
    Route::get('/mantenimiento/{id}/edit','Admin\MantenimientoController@getMantenimientoEdit')->name('mantenimiento_edit');
    Route::get('/mantenimiento/{id}/delete','Admin\MantenimientoController@getMantenimientoDelete')->name('mantenimiento_delete');
    Route::get('/mantenimiento/{id}/restore','Admin\MantenimientoController@getMantenimientoRestore')->name('mantenimiento_delete');
    Route::post('/mantenimiento/add','Admin\MantenimientoController@postMantenimientoAdd')->name('mantenimiento_add');
    Route::post('/mantenimiento/{id}/edit','Admin\MantenimientoController@postMantenimientoEdit')->name('mantenimiento_edit');
    Route::post('/mantenimiento/search','Admin\MantenimientoController@postMantenimientoSearch')->name('mantenimiento_search');
    Route::post('/mantenimiento/{id}/gallery/add','Admin\MantenimientoController@postMantenimientoGalleryAdd')->name('mantenimiento_gallery_add');
    Route::get('/mantenimiento/{id}/gallery/{gid}/delete','Admin\MantenimientoController@getMantenimientoGalleryDelete')->name('mantenimiento_gallery_delete');

    //categorias
    Route::get('/categories/{module}','Admin\CategoriesController@getHome')->name('categories');
    Route::post('/category/add','Admin\CategoriesController@postCategoryAdd')->name('categories_add');
    Route::get('/category/{id}/edit' , 'Admin\CategoriesController@getCategoryEdit')->name('categories_edit');
    Route::post('/category/{id}/edit' , 'Admin\CategoriesController@postCategoryEdit')->name('categories_edit');
    Route::get('/category/{id}/delete' , 'Admin\CategoriesController@getCategoryDelete')->name('categories_delete');

});
