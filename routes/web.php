<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login','ConnectContoller@getLogin')->name('login');
Route::post('/login','ConnectContoller@postLogin')->name('login');
Route::get('/recover','ConnectContoller@getRecover')->name('recover');
Route::post('/recover','ConnectContoller@postRecover')->name('recover');
Route::get('/register','ConnectContoller@getRegister')->name('register');
Route::post('/register','ConnectContoller@postRegister')->name('register');
Route::get('/logout','ConnectContoller@getLogout')->name('logout');
