@extends('admin.master')
@section('title','Dashboard')

@section('content')
    <div class="container-fluid">
        @if(kvfj(Auth::user()->permissions,'dashboard_small_stats'))
        <div class="panel shadow">
            <div class="header">
                <h2 class="title"><i class="far fa-chart-bar"></i> Estadisticas rápidas</h2>
            </div>
        </div>
        <div class="row mtop16">
            @if(kvfj(Auth::user()->permissions,'dashboard_small_users'))
            <div class="col-md-3">
                <div class="panel shadow">
                    <div class="header">
                        <h2 class="title"><i class="fas fa-users"></i> Usuarios registrados</h2>
                    </div>
                    <div class="inside">
                        <div class="big_count">{{ $users }}</div>
                    </div>
                </div>
            </div>
        @endif

            <div class="col-md-3">
                <div class="panel shadow">
                    <div class="header">
                        <h2 class="title"><i class="fas fa-boxes"></i> Productos Registrados</h2>
                    </div>
                    <div class="inside">
                        <div class="big_count">{{ $products }}</div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="panel shadow">
                    <div class="header">
                        <h2 class="title"><i class="fas fa-wrench"></i>Matenimientos registrados</h2>
                    </div>
                    <div class="inside">
                        <div class="big_count">{{ $mantenimientos }}</div>
                    </div>
                </div>
            </div>



            @endif

            <div class="col-md-3">
                <style>


                    .calendar {
                        margin: auto;
                        width: 220px;
                        background-color: #fff;
                        box-shadow: 0px 0px 15px 4px rgba(0, 0, 0, 0.2);

                    }

                    .month {
                        display: flex;
                        justify-content: space-between;
                        align-items: center;
                        width: 100%;
                        padding: 30px 30px;
                        text-align: center;
                        background-color: #2ecc71;
                        color: #fff;
                    }

                    .weekdays {
                        background-color: #27ae60;
                        color: #fff;
                        padding: 7px 0;
                        display: flex;
                    }

                    .days {
                        font-weight: 300;
                        padding: 10px 0;
                        display: flex;
                        flex-wrap: wrap;
                    }

                    .weekdays div,
                    .days div {
                        text-align: center;
                        width: 14.28%;
                    }

                    .days div {
                        padding: 10px 0;
                        margin-bottom: 10px;
                        transition: all 0.4s;
                    }

                    .prev_date {
                        color: #999;
                    }

                    .today {
                        background-color: #27ae60;
                        color: #fff;
                    }

                    .days div:hover {
                        cursor: pointer;
                        background-color: #dfe6e9
                    }

                    .prev,
                    .next {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        width: 50px;
                        height: 50px;
                        border-radius: 50%;
                        font-size: 23px;
                        background-color: rgba(0, 0, 0, 0.1);
                        transition: all 0.4s;
                    }

                    .prev:hover,
                    .next:hover {
                        cursor: pointer;
                        background-color: rgba(0, 0, 0, 0.2);
                    }

                    #month {
                        font-size: 30px;
                        font-weight: 500;
                    }
                </style>
            </head>


                <div class="wrapper">
                    <div class="calendar">
                        <div class="month">
                            <div class="prev" onclick="moveDate('prev')">
                                <span>&#10094;</span>
                            </div>
                            <div>
                                <h2 id="month"></h2>
                                <p id="date_str"></p>
                            </div>
                            <div class="next" onclick="moveDate('next')">
                                <span>&#10095;</span>
                            </div>
                        </div>
                        <div class="weekdays">
                            <div>Dom</div>
                            <div>Lun</div>
                            <div>Mar</div>
                            <div>Mir</div>
                            <div>Jue</div>
                            <div>Vier</div>
                            <div>Sab</div>
                        </div>
                        <div class="days">

                        </div>
                    </div>
                </div>
                <script>
                    var dt = new Date();
                    function renderDate() {
                        dt.setDate(1);
                        var day = dt.getDay();
                        var today = new Date();
                        var endDate = new Date(
                            dt.getFullYear(),
                            dt.getMonth() + 1,
                            0
                        ).getDate();

                        var prevDate = new Date(
                            dt.getFullYear(),
                            dt.getMonth(),
                            0
                        ).getDate();
                        var months = [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "Julio",
                            "Agosto",
                            "September",
                            "October",
                            "November",
                            "December"
                        ]
                        document.getElementById("month").innerHTML = months[dt.getMonth()];
                        document.getElementById("date_str").innerHTML = dt.toDateString();
                        var cells = "";
                        for (x = day; x > 0; x--) {
                            cells += "<div class='prev_date'>" + (prevDate - x + 1) + "</div>";
                        }
                        console.log(day);
                        for (i = 1; i <= endDate; i++) {
                            if (i == today.getDate() && dt.getMonth() == today.getMonth()) cells += "<div class='today'>" + i + "</div>";
                            else
                                cells += "<div>" + i + "</div>";
                        }
                        document.getElementsByClassName("days")[0].innerHTML = cells;

                    }

                    function moveDate(para) {
                        if(para == "prev") {
                            dt.setMonth(dt.getMonth() - 1);
                        } else if(para == 'next') {
                            dt.setMonth(dt.getMonth() + 1);
                        }
                        renderDate();
                    }
                </script>
            </div>
        </div>


    </div>
@endsection
