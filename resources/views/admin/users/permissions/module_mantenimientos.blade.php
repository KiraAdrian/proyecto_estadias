<div class="col-md-4 d-flex">
    <div class="panel shadow">
        <div class="header">
            <h2 class="title"><i class="fas fa-box-open"></i> Modulo de Mantenimientos </h2>
        </div>
            <div class="inside">
                <div class="form-check">
                    <input type="checkbox" value="true" name="mantenimientos"@if(kvfj($u->permissions,'mantenimientos'))
                    checked @endif> <label for="mantenimientos"> Puede ver el listado de productos.
                    </label>
                </div>

                <div class="form-check">
                    <input type="checkbox" value="true" name="mantenimiento_add"@if(kvfj($u->permissions,'mantenimiento_add'))
                    checked @endif> <label for="mantenimiento_add"> Puede agregar nuevos productos.
                    </label>
                </div>

                <div class="form-check">
                    <input type="checkbox" value="true" name="mantenimiento_edit"@if(kvfj($u->permissions,'mantenimiento_edit'))
                    checked @endif> <label for="mantenimiento_edit"> Puede editar producto.
                    </label>
                </div>

                <div class="form-check">
                    <input type="checkbox" value="true" name="mantenimiento_delete"@if(kvfj($u->permissions,'mantenimiento_delete'))
                    checked @endif> <label for="mantenimiento_delete">Puede eliminar producto
                    </label>
                </div>

                <div class="form-check">
                    <input type="checkbox" value="true" name="mantenimiento_search"@if(kvfj($u->permissions,'mantenimiento_search'))
                    checked @endif> <label for="mantenimiento_search">Puede buscar  mantenimientos
                    </label>
                </div>

                <div class="form-check">
                    <input type="checkbox" value="true" name="mantenimiento_gallery_add"@if(kvfj($u->permissions,'mantenimiento_gallery_add'))
                    checked @endif> <label for="mantenimiento_gallery_add">Puede agregar galeria de imagenes
                    </label>
                </div>

                <div class="form-check">
                    <input type="checkbox" value="true" name="mantenimiento_gallery_delete"@if(kvfj($u->permissions,'mantenimiento_gallery_delete'))
                    checked @endif> <label for="mantenimiento_gallery_delete">Puede eliminar galeria de imagenes
                    </label>
                </div>

            </div>
    </div>
</div>
