<div class="sidebar shadow">
    <div class="section-top">
        <div class="logo">
            <img src="{{ url('/static/images/logo3.png') }}" class="img-fluid">
        </div>

        <div class="user">
            <span class="subtitle">Hola:</span>
            <div class="name">
                {{Auth::user()->name}} {{Auth::user()->lastname}}
                <a href="{{ url('/logout') }}" data-toggle="tooltip" data-placement="top" title="Cerrar sesión">
                    <i class="fas fa-sign-out-alt"></i></a>
            </div>
            <div class="email">{{Auth::user()->email}}</div>
        </div>
    </div>
    <div class="main">
        <ul>
            @if(kvfj(Auth::user()->permissions,'dashboard'))
            <li>
                <a href="{{ url('/admin') }}" class="lk-dashboard"><i class="fas fa-house-user"></i>Dashboard</a>
            </li>
            @endif

            @if(kvfj(Auth::user()->permissions,'products'))
            <li>
                <a href="{{ url('/admin/products/1') }}" class="lk-products lk-product_add lk-product_edit
                lk-product_gallery_add "><i class="fas fa-box-open"></i>Productos</a>
            </li>
            @endif

            @if(kvfj(Auth::user()->permissions,'mantenimientos'))
            <li>
                <a href="{{ url('/admin/mantenimientos/1') }}" class="lk-mantenimiento lk-mantenimiento_add lk-mantenimiento_edit
                lk-mantenimiento_gallery_add"><i class="fas fa-tools"></i>Mantenimientos</a>
            </li>
            @endif

            @if(kvfj(Auth::user()->permissions,'categories'))
            <li>
                <a href="{{ url('/admin/categories/0') }}" class="lk-categories lk-category_add
               lk-category_edit lk-category_delete "><i class="fas fa-folder"></i>Categorias</a>
            </li>
            @endif

            @if(kvfj(Auth::user()->permissions,'user_list'))
            <li>
                <a href="{{ url('/admin/users/all') }}" class="lk-user_list lk-user_edit"><i class="fas fa-user-friends"></i></i>Usuarios</a>
            </li>
            @endif

        </ul>
    </div>
</div>
