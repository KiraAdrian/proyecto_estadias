@extends('admin.master')
@section('title','Mantenimientos')

@section('breadcrumb')
    <li class="breadcrumb-item">
    <a href="{{ url('/admin/mantenimientos/all') }}"><i class="fas fa-tools"></i> Productos en Mantenimientos</a>
    </li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="panel shadow">
            <div class="header">
                <h2 class="title"><i class="fas fa-boxes"></i> Productos</h2>
                <ul>
                    @if(kvfj(Auth::user()->permissions,'mantenimiento_add'))
                    <li>
                        <a href="{{ url('admin/mantenimiento/add') }}" >
                            <i class="fas fa-plus"></i>
                            Agragar Producto</a>
                    </li>
                    @endif
                    <li>
                        <a href="#">Filtrar <i class="fas fa-chevron-down"></i></a>
                        <ul class="shadow">
                            <li> <a href="{{ url('/admin/mantenimientos/1') }}"><i class="fas fa-upload"></i> Públicos</a></li>
                            <li> <a href="{{ url('/admin/mantenimientos/0') }}"><i class="fas fa-eraser"></i> Borrador</a></li>
                            <li> <a href="{{ url('/admin/mantenimientos/trash') }}"><i class="fas fa-trash-restore"></i> Papelera</a></li>
                            <li> <a href="{{ url('/admin/mantenimientos/all') }}"><i class="fas fa-boxes"></i> Todos</a></li>
                        </ul>
                    </li>
                    @if(kvfj(Auth::user()->permissions,'mantenimiento_search'))
                    <li>

                        <a href="#" id="btn_search">
                            <i class="fas fa-search"></i>Buscar
                        </a>

                    </li>
                    @endif
                </ul>
            </div>
                <div class="inside">
                    <div class="form_search" id="form_search">
                        {!! Form::open(['url'=>'/admin/mantenimiento/search']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                {!! Form::text('search',null,['class'=>'form-control','placeholder'=>'Ingresa su busqueda']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('filter',['0'=>'Nombre del producto','1'=>'Marca'],0,['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::select('status',['0'=>'Borrador','1'=>'Pùblicos'],0,['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::submit('Buscar',['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td></td>
                                <td>Nombre</td>
                                <td>Categoria</td>
                                <td>Fecha de registro</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mantenimientos as $m)
                                <tr>
                                    <td width="50">{{ $m->id }}</td>
                                    <td width="64">
                                        <a href="{{ url('/uploads/'.$m->file_path.'/'.$m->image) }}" data-fancybox="gallery">
                                            <img src="{{ url('/uploads/'.$m->file_path.'/t_'.$m->image) }}" width="64">
                                        </a>
                                    </td>
                                    <td>{{ $m->name }} @if($m->status=="0")<i class="fas fa-eraser" data-toggle="tooltip" data-placement="right" title="Estado:Borrador"></i>  @endif</td>
                                    <td>{{ $m->cat->name }}</td>
                                    <td>{{ $m->dia }}</td>
                                    <td>
                                        <div class="opts">
                                            @if(kvfj(Auth::user()->permissions,'mantenimiento_edit'))
                                            @if(is_null($m->deleted_at))
                                            <a href="{{ url('/admin/mantenimiento/'.$m->id.'/edit') }} "
                                                data-toggle="tooltip" data-placement="left" title="Editar">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            @endif
                                            @endif

                                            @if(kvfj(Auth::user()->permissions,'mantenimiento_delete'))
                                                @if(is_null($m->deleted_at))
                                                 <a href="#" data-action="delete" data-path="admin/mantenimiento"
                                                    data-object="{{ $m->id }}" data-toggle="tooltip" data-placement="right" title="Eliminar"
                                                    class="btn-deleted">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>

                                                @else
                                                <a href="{{ url('/admin/mantenimiento/'.$m->id.'/restore') }}" data-action="restore" data-path="admin/mantenimiento"
                                                    data-object="{{ $m->id }}" data-toggle="tooltip" data-placement="right" title="Restaurar"
                                                    class="btn-deleted">
                                                    <i class="fas fa-window-restore"></i>
                                                </a>
                                                @endif
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            {{-- <tr>
                                <td colspan="7">{!! $products->render() !!}</td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>

        </div>
    </div>
@endsection

