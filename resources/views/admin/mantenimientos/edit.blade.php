@extends('admin.master')
@section('title','Editar Mantenimiento')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ url('/admin/mantenimientos/all') }}"><i class="fas fa-tools"></i> Productos en Mantenimientos</a>
    </li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="panel shadow">
                    <div class="header">
                        <h2 class="title"> <i class="fas fa-edit"></i> Editar Producto en mantenimiento</h2>
                    </div>
                    <div class="inside">
                        {{ Form::open(['url'=>'/admin/mantenimiento/'.$m->id.'/edit','files'=> true]) }}
                        <div class="row">
                            <div class="col-md-6">
                                <label for="name">Nombre del producto en mantenimiento</label>
                                <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fas fa-keyboard"></i>
                                            </span>
                                        </div>
                                        {!! Form::text('name',$m->name,['class'=>'form-control']) !!}
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label for="category">Categoría</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="fas fa-keyboard"></i>
                                        </span>
                                    </div>
                                    {!! Form::select('category', $cats,$m->category_id,['class'=>'custom-select']) !!}
                            </div>
                            </div>

                            <div class="col-md-3">
                                <label for="name">Imagen Destacada</label>
                                <div class="custom-file">
                                {!! Form::file('img',['class'=>'custom-file-input','id'=>'customFile','accept'=>'image/*']) !!}
                                <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                        </div>

                        <div class="row mtop16">
                            <div class="col-md-3">
                                <label for="brand">Marca</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="fas fa-keyboard"></i>
                                        </span>
                                    </div>
                                    {!! Form::text('brand',$m->brand,['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="cantidad">Fecha de registro</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="fas fa-keyboard"></i>
                                        </span>
                                    </div>
                                    {{ Form::date('dia', $m->dia, ['class' => ($errors->has('dia')) ? 'form-control is-invalid' : 'form-control']) }}
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label for="codigo">Estado</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="far fa-keyboard"></i>
                                            </span>
                                        </div>
                                        {{ Form::select('status',['1'=>'Publico','0'=>'Borrado'],$m->status,['class'
                                        =>'custom-select']) }}
                                    </div>
                            </div>
                        </div>

                        <div class="row mtop16">
                            <div class="col-md-12">
                                <label for="content">Descripción</label>
                                {!! Form::textarea('content',$m->content,['class'=>'form-control','id'=>'editor']) !!}
                            </div>
                        </div>

                        <div class="row mtop16">
                            <div class="col-md-12">
                                {!! Form::submit('Guardar',['class' => 'btn btn-success']) !!}
                            </div>

                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col md-3">
                <div class="panel shadow">
                    <div class="header">
                        <h2 class="title"> <i class="fas fa-image"></i> imagen destacada</h2>
                        <div class="inside">
                            <img src="{{ url('/uploads/'.$m->file_path.'/'.$m->image) }}" class="img-fluid" >
                        </div>
                    </div>
                </div>
                <div class="panel shadow mtop16">
                    <div class="header">
                        <h2 class="title"> <i class="fas fa-images"></i> Galeria</h2>
                    </div>
                    <div class="inside product_gallery">
                        @if(kvfj(Auth::user()->permissions,'mantenimiento_gallery_add'))
                            {!! Form::open(['url'=>'/admin/mantenimiento/'.$m->id.'/gallery/add','files'=> true,
                            'id'=>'form_product_gallery']) !!}
                            {!! Form::file('file_image',['id'=>'product_file_image','accept'=>'image/*',
                            'style' => 'display: none;','required']) !!}
                            {!! Form::close() !!}
                    <div class="btn-submit">
                        <a href="#" id="btn_product_file_image"><i class="fas fa-plus"></i></a>
                    </div>
                    @endif
                    <div class="tums">
                        @foreach ($m->getGallery as $img)
                        <div class="tumb">
                            @if(kvfj(Auth::user()->permissions,'mantenimiento_gallery_delete'))
                            <a
                            href="{{ url('/admin/mantenimiento/'.$m->id.'/gallery/'.$img->id.'/delete') }}"
                                data-toggle="tooltip" data-placement="right" title="Eliminar">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            @endif
                            <img src="{{ url('/uploads/'.$img->file_path.'/t_'.$img->file_name) }}" >

                        </div>
                        @endforeach
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

