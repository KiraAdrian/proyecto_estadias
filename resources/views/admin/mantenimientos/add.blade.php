@extends('admin.master')
@section('title','Agrgar Producto')

@section('breadcrumb')
    <li class="breadcrumb-item">
    <a href="{{ url('/admin/mantenimientos/all') }}"><i class="fas fa-boxes"></i> Productos</a>
    </li>

    <li class="breadcrumb-item">
        <a href="{{ url('/admin/mantenimiento/add') }}"><i class="fas fa-plus"></i> Agregar Producto</a>
        </li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="panel shadow">
            <div class="header">
                <h2 class="title"> <i class="fas fa-plus"></i> Agragar Producto en Mantenimiento</h2>
            </div>
            <div class="inside">
                {{ Form::open(['url'=>'/admin/mantenimiento/add','files'=> true]) }}
                <div class="row">
                    <div class="col-md-6">
                        <label for="name">Nombre del producto en mantenimiento</label>
                        <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">
                                        <i class="fas fa-keyboard"></i>
                                    </span>
                                </div>
                                {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="category">Categoría</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                            {!! Form::select('category', $cats,0,['class'=>'custom-select']) !!}
                    </div>
                    </div>

                    <div class="col-md-3">
                        <label for="name">Imagen Destacada</label>
                        <div class="custom-file">
                        {!! Form::file('img',['class'=>'custom-file-input','id'=>'customFile','accept'=>'image/*']) !!}
                        <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                </div>

                <div class="row mtop16">
                    <div class="col-md-3">
                        <label for="brand">Marca</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                            {!! Form::text('brand',null,['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="cantidad">Fecha de registro</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">
                                    <i class="fas fa-keyboard"></i>
                                </span>
                            </div>
                            {{ Form::date('dia', null, ['class' => ($errors->has('dia')) ? 'form-control is-invalid' : 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="row mtop16">
                    <div class="col-md-12">
                        <label for="content">Descripción</label>
                        {!! Form::textarea('content',null,['class'=>'form-control','id'=>'editor']) !!}
                    </div>
                </div>

                <div class="row mtop16">
                    <div class="col-md-12">
                        {!! Form::submit('Guardar',['class' => 'btn btn-success']) !!}
                    </div>

                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

