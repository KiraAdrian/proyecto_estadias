<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ProtecUT</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
		 <link rel="stylesheet" href="CSS/fontello.css">
        <link rel="stylesheet" href="css/sty.css">
    </head>
    <header>
        <div class="contenedor">
            <h1 class="icon-dog">ProtectUT</h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
                    <nav class="menu">
                        @if (Route::has('login'))
                            @auth
                                <a href="{{ url('/admin') }}">Regresar</a>
                            @else
                                <a href="{{ route('login') }}">Iniciar Sesión</a>
                                <a href="{{ route('register') }}">Registrar</a>
                            @endauth
                    @endif
                    </nav>
        </div>
    </header>
            <main>
                <section id="banner">
                    <img src="images/woow.jpg">
                    <div class="contenedor">
                        <h2>Sistema de gestor de mantenimiento</h2>

                    </div>
                </section>

                <section id="bienvenidos">
                <div class="contenedor">
                    <h2>BIENVENIDOS A Nuestra PAGINA WEB</h2>
                    </div>
                </section>

                <section id="blog">
                    <h3>LOS OBJETIVOS DE LA ORGANIZACIÓN DE MANTENIMIENTO</h3>
                    <div class="contenedor">
                        <article>
                            <img src="images/confianza.jpg">
                            <h4>MANTENIMIENTO Y CONFIABILIDAD.</h4>
                        </article>
                        <article>
                            <img src="images/tall.jpg">
                            <h4>PROCESOS BASE DE MANTENIMIENTO.</h4>
                        </article>
                        <article>
                            <img src="images/plallt.jpg">
                            <h4>LA PLANIFICACIÓN DE MANTENIMIENTO</h4>
                        </article>
                    </div>
                </section>

                <section id="info">

                    <div class="contenedor">
                        <div class="info-pet">
                            <img src="images/img1.jpg" alt="">

                        </div>
                        <div class="info-pet">
                            <img src="images/img2.jpg" alt="">

                        </div>
                        <div class="info-pet">
                            <img src="images/img3.jpg" alt="">

                        </div>
                        <div class="info-pet">
                            <img src="images/img4.jpg" alt="">

                        </div>
                    </div>
                </section>
            </main>

            <footer>
                <div class="contenedor">
                    <p class="copy">ProtectUT &copy; 2020</p>
                    <div class="sociales">
                        <a class="fontawesome-facebook-sign" href="#"></a>
                        <a class="fontawesome-twitter" href="#"></a>
                        <a class="fontawesome-google-plus-sign" href="#"></a>
                    </div>
                </div>
            </footer>
</html>
