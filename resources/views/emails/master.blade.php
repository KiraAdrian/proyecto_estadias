<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="">
</head>
<body style="margin: 0px; padding: 5px; background-color:#D3F2F8 ">

    <div style="display: block;
    max-width: 728px;
    margin: 0px auto;
    width: 60%">

        <img src="{{ url('/static/images/reserve.jpg') }}" style="width: 100%; display: block" ">

        <div style="background-color: #ffffff;
        padding: 24px">
            @yield('content')
        </div>
    </div>
</body>
</html>
