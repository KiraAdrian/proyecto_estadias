<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    public function __Construct(){
        $this->middleware('auth');
        $this->middleware('user.status');
        $this->middleware('user.permissions');
        $this->middleware('isadmin');
    }

    public function getUsers($status){
        if($status == 'all'):
            $users=User::orderBy('id','Desc')->paginate(20);
        else:
            $users =User::where('status', $status)->orderBy('id','Desc')->paginate(20);
        endif;
        $data=['users'=>$users];
        return view('admin.users.home',$data);
    }

    public function getUserEdit($id){
        $u = User::findOrFail($id);
        $data = ['u' => $u];
        return view('admin.users.user_edit',$data);
    }

    public function postUserEdit(Request $request, $id){
        $u = User::findOrFail($id);
        $u->role= $request->input('user_type');
         if($request->input('user_type') == '1'):
            if(is_null($u->permissions)):
                $permissions=[
                    'dashboard'=>true
                ];
                $permissions = json_encode($permissions);
                $u->permissions = $permissions;
            endif;
        else:
            $u->permissions=null;
        endif;
        if($u->save()):
            if($request->input('user_type') == '1'):
                return redirect('/admin/user/'.$u->id.'/permissions')->with('El rango del usuario, se actulizo con éxito')->with('typealert','success');
            else:
                return back()->with('message','El rango del usuario, se actulizo con éxito')->with('typealert','success');
        endif;
    endif;
}

    public function getUserBanned($id){
        $u = User::findOrFail($id);
        if($u->status == "100"):
            $u->status = "1";
            $msg="Usuario activado nuevamente";
        else:
            $u->status = "100";
            $msg="Usuario suspendido con éxito";
        endif;

        if($u->save()):
            return back()->with('message',$msg)->with('typealert','success');
        endif;
    }

    public function getUserPermissions($id){
        $u = User::findOrFail($id);
        $data = ['u' => $u];
        return view('admin.users.user_permissions',$data);
    }

    public function postUserPermissions(Request $request, $id){
        $u = User::findOrFail($id);
        $permissions=[
            'dashboard'=>$request->input('dashboard'),
            'dashboard_small_stats'=>$request->input('dashboard_small_stats'),
            'dashboard_small_users'=>$request->input('dashboard_small_users'),


            'mantenimientos'=>$request->input('mantenimientos'),
            'mantenimiento_add'=>$request->input('mantenimiento_add'),
            'mantenimiento_edit'=>$request->input('mantenimiento_edit'),
            'mantenimiento_delete'=>$request->input('mantenimiento_delete'),
            'mantenimiento_search'=>$request->input('mantenimiento_search'),
            'mantenimiento_gallery_add'=>$request->input('mantenimiento_gallery_add'),
            'mantenimiento_gallery_delete'=>$request->input('mantenimiento_gallery_delete'),

            'products'=>$request->input('products'),
            'product_add'=>$request->input('product_add'),
            'product_edit'=>$request->input('product_edit'),
            'product_delete'=>$request->input('product_delete'),
            'product_search'=>$request->input('product_search'),
            'product_gallery_add'=>$request->input('product_gallery_add'),
            'product_gallery_delete'=>$request->input('product_gallery_delete'),

            'categories'=>$request->input('categories'),
            'categories_add'=>$request->input('categories_add'),
            'categories_edit'=>$request->input('categories_edit'),
            // 'categories_delete'=>$request->input('categories_delete'),
            'user_list'=>$request->input('user_list'),
            'user_edit'=>$request->input('user_edit'),
            'user_banned'=>$request->input('user_banned'),
            'user_permissions'=>$request->input('user_permissions')

        ];

        $permissions = json_encode($permissions);
        $u->permissions = $permissions;
        if($u->save()):
            return back()->with('message','Los permisos del usuario fueron actualizados con éxito')->with('typealert','success');
        endif;
    }
}
