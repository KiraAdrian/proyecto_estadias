<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\Category , App\Http\Models\Mantenimiento, App\Http\Models\MGalley;
use Validator ,Str ,Config, Image;

class MantenimientoController extends Controller
{
    public function __Construct(){
        $this->middleware('auth');
        $this->middleware('user.status');
        $this->middleware('user.permissions');
        $this->middleware('isadmin');
    }

    public function getHome($status){
        switch ($status) {
            case '0':
                $mantenimientos = Mantenimiento::with(['cat'])->where('status','0')->orderBy('id','desc')->paginate(25);
                break;
            case '1':
                $mantenimientos = Mantenimiento::with(['cat'])->where('status','1')->orderBy('id','desc')->paginate(25);
            break;

            case 'all':
                $mantenimientos = Mantenimiento::with(['cat'])->orderBy('id','desc')->paginate(25);
            break;

            case 'trash':
                $mantenimientos = Mantenimiento::with(['cat'])->onlyTrashed()->orderBy('id','desc')->paginate(25);
            break;
                break;
        }

        $data = ['mantenimientos' => $mantenimientos];
        return view('admin.mantenimientos.home', $data);
    }


    public function getMantenimientoAdd(){
        $cats=Category::where('module','0')->pluck('name','id');
        $data=['cats'=> $cats];
        return view('admin.mantenimientos.add', $data);
    }

    public function postMantenimientoAdd(Request $request){
        $rules=[
            'name'=>'required',
            'img'=>'required|image',
            'content'=>'required',
            'brand'=>'required',
            'dia'=>'required',
        ];

        $messages=[
            'name.required'=>'El nombre del producto es requerido',
            'img.required'=>'Seleccione una imagen destacada',
            'img.image'=>'El archivo no es una imagen',
            'brand.required'=>'La marca del producto es requerido',
            'content.required'=>'La descripción del producto es requerido',
            'dia.required'=>'La fecha es requerida',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error.')->with(
                'typealert','danger')->withInput();
            else:
                $path= '/'.date('Y-m-d');
                $fileExt = trim($request->file('img')->getClientOriginalExtension());
                $upload_path = Config::get('filesystems.disks.uploads.root');
                $name=Str::slug(str_replace($fileExt,'', $request->file('img')->getClientOriginalName()));

                $filename = rand(1,999).'-'.$name.'.'.$fileExt;
                $file_file = $upload_path.'/'.$path.'/'.$filename;


                $mantenimiento = new Mantenimiento;
                $mantenimiento->status = '0';
                $mantenimiento->name = e($request->input('name'));
                $mantenimiento->slug = Str::slug($request->input('name'));
                $mantenimiento->category_id = $request->input('category');
                $mantenimiento->file_path= date('Y-m-d');
                $mantenimiento->image = $filename;
                $mantenimiento->dia=$request->input('dia');
                $mantenimiento->brand = e($request->input('brand'));
                $mantenimiento->content = e($request->input('content'));

                if($mantenimiento->save()):
                    if($request->hasFile('img')):
                        $fl = $request->img->storeAs($path,$filename,'uploads');
                        $img = Image::make($file_file);
                        $img->fit(256,256, function($constraint){
                            $constraint->upsize();
                        });
                        $img->save($upload_path.'/'.$path.'/t_'.$filename);
                    endif;
                    return redirect('/admin/mantenimientos/all')->with('message','Guardado con éxito.')->with(
                        'typealert','success');
                endif;
            endif;

    }

    public function getMantenimientoEdit($id){
        $m = Mantenimiento::findOrFail($id);
        $cats=Category::where('module','0')->pluck('name','id');
        $data=['cats'=> $cats,'m'=>$m];
        return view('admin.mantenimientos.edit',$data);
    }
    public function postMantenimientoEdit($id, Request $request){
        $rules=[
            'name'=>'required',
            'content'=>'required',
            'img'=>'image',
            'brand'=>'required',
            'dia'=>'required',
        ];
        $messages=[
            'name.required'=>'El nombre del producto es requerido',
            'img.image'=>'El archivo no es una imagen',
            'brand.required'=>'La marca del producto es requerido',
            'content.required'=>'La descripción del producto es requerido',
            'dia.required'=>'La fecha es requerida',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error.')->with(
                'typealert','danger')->withInput();
            else:
                $mantenimiento = Mantenimiento::findOrFail($id);
                $ipp = $mantenimiento->file_path;
                $ip= $mantenimiento->image;
                $mantenimiento->status = e($request->input('status'));
                $mantenimiento->name = e($request->input('name'));
                $mantenimiento->category_id = $request->input('category');
                if($request->hasFile('img')):
                    $path= '/'.date('Y-m-d');
                    $fileExt = trim($request->file('img')->getClientOriginalExtension());
                    $upload_path = Config::get('filesystems.disks.uploads.root');
                    $name=Str::slug(str_replace($fileExt,'', $request->file('img')->getClientOriginalName()));

                    $filename = rand(1,999).'-'.$name.'.'.$fileExt;
                    $file_file = $upload_path.'/'.$path.'/'.$filename;

                    $mantenimiento->file_path= date('Y-m-d');
                    $mantenimiento->image = $filename;
                endif;
                $mantenimiento->dia=$request->input('dia');
                $mantenimiento->brand = e($request->input('brand'));
                $mantenimiento->content = e($request->input('content'));

                if($mantenimiento->save()):
                    if($request->hasFile('img')):
                        $fl = $request->img->storeAs($path,$filename,'uploads');
                        $img = Image::make($file_file);
                        $img->fit(256,256, function($constraint){
                            $constraint->upsize();
                        });
                        $img->save($upload_path.'/'.$path.'/t_'.$filename);
                        unlink($upload_path.'/'.$ipp.'/'.$ip);
                        unlink($upload_path.'/'.$ipp.'/t_'.$ip);
                    endif;
                    return back()->with('message','Actualizado con éxito.')->with(
                        'typealert','success');
                endif;
            endif;

    }

    public function postMantenimientoGalleryAdd($id, Request $request){

        $rules=[
            'file_image'=>'required|image',

        ];

        $messages=[
            'file_image.required'=>'Seleccione una imagen ',
            'file_image.image'=>'El archivo no es una imagen',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error.')->with(
                'typealert','danger')->withInput();
            else:
                if($request->hasFile('file_image')):
                    $path= '/'.date('Y-m-d');
                    $fileExt = trim($request->file('file_image')->getClientOriginalExtension());
                    $upload_path = Config::get('filesystems.disks.uploads.root');
                    $name=Str::slug(str_replace($fileExt,'', $request->file('file_image')->getClientOriginalName()));

                    $filename = rand(1,999).'-'.$name.'.'.$fileExt;
                    $file_file = $upload_path.'/'.$path.'/'.$filename;

                    $mg=new MGalley;
                    $mg->mantenimiento_id = $id;
                    $mg->file_path = date('Y-m-d');
                    $mg->file_name= $filename;

                    if($mg->save()):
                        if($request->hasFile('file_image')):
                            $fl = $request->file_image->storeAs($path,$filename,'uploads');
                            $img = Image::make($file_file);
                            $img->fit(256,256, function($constraint){
                                $constraint->upsize();
                            });
                            $img->save($upload_path.'/'.$path.'/t_'.$filename);
                        endif;
                        return back()->with('message','Imagen sudida con éxito.')->with(
                            'typealert','success');
                    endif;
                endif;
                endif;

    }

    function  getMantenimientoGalleryDelete($id , $gid){
        $mg = MGalley::findOrFail($gid);
        $path = $mg->file_path;
        $file = $mg->file_name;
        $upload_path = Config::get('filesystems.disks.uploads.root');
        if($mg->mantenimiento_id != $id){
            return back()->with('message','La Imagen no se puede Eliminar.')->with(
                'typealert','danger');
        }else{
            if($mg->delete()):
                unlink($upload_path.'/'.$path.'/'.$file);
                unlink($upload_path.'/'.$path.'/t_'.$file);
            return back()->with('message','Imagen borrada con éxisto.')->with(
                    'typealert','success');
            endif;
        }
    }


    public function postMantenimientoSearch(Request $request){
        $rules=[
            'search'=>'required',
        ];
        $messages=[
            'search.required'=>'El campo consulta es requerido',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error.')->with(
                'typealert','danger')->withInput();
            else:
                switch ($request->input('filter')):
                    case '0':
                        $mantenimientos = Mantenimiento::with(['cat'])->where('name','LIKE',   '%'.$request->input('search').'%')->where('status',$request->input('status'))->orderBy('id','desc')->get();
                        break;
                    case '1':
                        $mantenimientos  = Mantenimiento::with(['cat'])->where('brand',$request->input('search'))->orderBy('id','desc')->get();
                        break;
                endswitch;
                $data = ['mantenimientos' =>  $mantenimientos ];
                return view('admin.mantenimientos.search',$data);

            endif;
    }

    public function getMantenimientoDelete($id){
        $m = Mantenimiento::findOrFail($id);
        if($m->delete()):
            return back()->with('message','Produdcto en mantenimiento enviado a la papelera de reciclaje.')->with(
                'typealert','success')->withInput();
        endif;
    }

    public function getMantenimientoRestore($id){
        $m = Mantenimiento::onlyTrashed()->where('id',$id)->first();
        if($m->restore()):
            return redirect('/admin/mantenimiento/'.$m->id.'/edit')->with('message','Este producto en mantenimiento se restauro con éxito.')->with(
                'typealert','success')->withInput();
        endif;
    }

}
