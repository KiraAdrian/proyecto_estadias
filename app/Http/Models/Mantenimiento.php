<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mantenimiento extends Model
{
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $table='mantenimientos';
    protected $hidden=['created_at','updated_at'];

    public function cat(){
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function getGallery(){
        return $this->hasMany(MGalley::class,'mantenimiento_id','id',);
    }
}
